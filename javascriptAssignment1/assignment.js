//-----Function for Body's onload event populating Select box------------------------------ 
	function loadProvinces()
		{
			var provArray=["Alberta","Ontario",
                           "Saskatchewan","Quebec","Northwest Terroteries",
                           "British Columbia"];
			cboProv.innerHTML="<option name='province' value=''>-Select-</option>";
			for (provIndex=0;provIndex<provArray.length;provIndex++)
                {
                   cboProv.innerHTML+= "<option name=' province"+ provIndex + "' value='" + provArray[provIndex]+ "'>"+ provArray[provIndex]+"</option>";

                }
        }
//-----Function for submit button onclick event for Validating textboxes and Select box------  
    function validateForm()
		{   
            // Declaring global variables inside this function
             // so that these can be used in change() and loadProvinces()
            cboProv = document.getElementById('cboProv');
            txtName = document.getElementById('txtName');
            txtEmail = document.getElementById('txtEmail');
            emailRegex = /\b(\w+\.)*\w+@([a-zA-Z]+\.)+[a-zA-Z]{2,6}\b/;    

			if(cboProv.value=="")
     			{
     				alert('Please Select your Province');
                    cboProv.focus();
                    cboProv.style.backgroundColor='lightpink';
                    return;
                }
            else if(txtName.value=="")
     			{
     				alert('Please Enter your Name');
                    txtName.focus();
                    txtName.style.backgroundColor='lightpink';
 					return;
     			}
            else if(txtEmail.value=="")
                {
                    alert('Please Enter your Email Id');
                    txtEmail.focus();
                    txtEmail.style.backgroundColor='lightpink';
                    return;

                }
            else if (!emailRegex.test(txtEmail.value))
               {
                   alert('Please Enter Valid Email Id\nFormat should be abc@example.com');
                   txtEmail.focus();
                   return;
               }
            else
               {
                   alert("You have filled all detailes correctley.\n Form is Submitted.\n Have a good Day");
               }
       }
    function change()
       {
            if(cboProv.value!="")
                {
                    cboProv.style.backgroundColor='white';
                }
            if(txtName.value!="")
                {
                    txtName.style.backgroundColor='white';
                }
            if(emailRegex.test(txtEmail.value))
                {
                    txtEmail.style.backgroundColor='white';
                }
        }